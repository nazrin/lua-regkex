#include <stdbool.h>
#include <ctype.h>
#include <lua.h>
#include <lauxlib.h>
#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>

#define REG_MT "nazrin.regkex"
#define pushFail(L, err) { lua_pushnil(L); lua_pushstring(L, err); }

struct Regkex{
	pcre2_code* re;
	pcre2_match_data* md;
	PCRE2_SIZE* o;
} typedef Regkex;
struct Flag{
	char c;
	uint32_t f;
} typedef Flag;

static Regkex* getRegex(lua_State* L, int i){
	return ((Regkex*)luaL_checkudata(L, i, REG_MT));
}
static lua_Integer getOffset(lua_State* L, int i, size_t len){
	lua_Integer off = luaL_optinteger(L, i, 0);
	luaL_argcheck(L, (size_t)off <= len && off >= 0, 3, "Invalid offset");
	return off;
}
static uint32_t getFlags(lua_State* L, int i, const Flag* list, uint32_t flags){
	const char* fla = luaL_optstring(L, i, "");
	for(int o = 0; fla[o]; o++){
		char c = fla[o];
		for(int f = 0; list[f].c ; f++){
			if(c == list[f].c){
				flags |= list[f].f;
				goto next;
			} else if(c == toupper(list[f].c)){
				flags &= ~list[f].f;
				goto next;
			}
		}
		luaL_error(L, "Invalid flag: %q '%c'", fla, c);
		next: (void)0;
	}
	return flags;
}
static int doMatch(lua_State* L, Regkex* reg, const char* str, size_t len, size_t off){
	int r = pcre2_match(reg->re, (const unsigned char*)str, len, off, 0, reg->md, NULL);
	if(r <= 0){
		if(r == PCRE2_ERROR_NOMATCH)
			return 0;
		PCRE2_UCHAR buffer[256];
		pcre2_get_error_message(r, buffer, sizeof(buffer));
		pushFail(L, (const char*)buffer);
		return -1;
	}
	if(reg->o[0] > reg->o[1]){
		pushFail(L, "End ahead of start");
		return -1;
	}
	return r;
}

static void tablePushMatches(lua_State* L, const char* str, int r, const PCRE2_SIZE* o, bool find, bool single){
	if(!single)
		lua_newtable(L);
	for(int i = 0; i < r; i++){
		PCRE2_SIZE s = o[2*i], e = o[(2*i)+1];
		if(find){
			lua_newtable(L);
			lua_pushinteger(L, s + 1);
			lua_rawseti(L, -2, 1);
			lua_pushinteger(L, e);
			lua_rawseti(L, -2, 2);
		} else {
			lua_pushlstring(L, str + s, e - s);
		}
		if(single)
			return;
		lua_rawseti(L, -2, i+1);
	}
	if(!find){
		lua_pushinteger(L, o[0] + 1);
		lua_setfield(L, -2, "index");
	}
}
static void bufferPushReplace(lua_State* L, luaL_Buffer* b, const char* str, int r, const PCRE2_SIZE* o, const char* rStr, size_t rStrLen, size_t* t){
	if(o[0] > 0)
		luaL_addlstring(b, str + *t, o[0] - *t);
	if(rStr){
		luaL_addlstring(b, rStr, rStrLen);
	} else {
		lua_pushvalue(L, 3);
		for(int i = 0; i < r; i++){
			PCRE2_SIZE s = o[2*i], e = o[(2*i)+1];
			lua_pushlstring(L, str + s, e - s);
		}
		lua_pushinteger(L, o[0] + 1);
		lua_call(L, r + 1, 1);
		luaL_checktype(L, -1, LUA_TSTRING);
		luaL_addvalue(b);
	}
	*t += o[1] - *t;
}
static int advance(Regkex* reg, const char* str, size_t len, bool utf8, bool crlfIsNewline){
	start: (void)0;
	PCRE2_SIZE* o = reg->o;
	uint32_t options = 0;
	PCRE2_SIZE startOffset = o[1];
	if(o[0] == o[1]){
		if(o[0] == len)
			return 0;
		options = PCRE2_NOTEMPTY_ATSTART | PCRE2_ANCHORED;
	} else {
		PCRE2_SIZE startChar = pcre2_get_startchar(reg->md);
		if(startOffset <= startChar){
			if(startChar >= len)
				return 0;
			startOffset = startChar + 1;
			if(utf8){
				for(; startOffset < len; startOffset++)
					if((str[startOffset] & 0xc0) != 0x80)
						break;
			}
		}
	}
	int r = pcre2_match(reg->re, (const unsigned char*)str, len, startOffset, options, reg->md, NULL);
	if(r == PCRE2_ERROR_NOMATCH){
		if(options == 0)
			return 0;
		o[1] = startOffset + 1;
		if(utf8){
			while(o[1] < len){
				if((str[o[1]] & 0xc0) != 0x80)
					break;
				o[1]++;
			}
		} else if(crlfIsNewline && startOffset < len - 1 && str[startOffset] == '\r' && str[startOffset + 1] == '\n'){
			o[1]++;
		}
		goto start;
	}
	if(r < 0)
		return -1;
	if(o[0] > o[1])
		return -1;
	return r;
}
static int regSub(lua_State* L, bool global){
	Regkex* reg = getRegex(L, 1);
	size_t len; const char* str = luaL_checklstring(L, 2, &len);
	lua_Integer off = getOffset(L, 4, len);
	const char* rStr;
	size_t rStrLen = 0;
	switch(lua_type(L, 3)){
		case LUA_TSTRING:
			rStr = lua_tolstring(L, 3, &rStrLen); break;
		case LUA_TFUNCTION:
			rStr = NULL; break;
		default:
			luaL_argerror(L, 3, "Expected string or function");
			return 0;
	}

	int r = doMatch(L, reg, str, len, off);
	if(r == 0){
		lua_pushvalue(L, 2);
		return 1;
	} else if(r < 0){
		return 2;
	}

	luaL_Buffer b;
	luaL_buffinit(L, &b);

	size_t t = 0;
	bufferPushReplace(L, &b, str, r, reg->o, rStr, rStrLen, &t);

	if(!global){
		luaL_addlstring(&b, str + reg->o[1], len - reg->o[1]);
		luaL_pushresult(&b);
		return 1;
	}

	uint32_t optionsBits, newline;
	pcre2_pattern_info(reg->re, PCRE2_INFO_ALLOPTIONS, &optionsBits);
	bool utf8 = !!(optionsBits & PCRE2_UTF);
	pcre2_pattern_info(reg->re, PCRE2_INFO_NEWLINE, &newline);
	bool crlfIsNewline = newline == PCRE2_NEWLINE_ANY || newline == PCRE2_NEWLINE_CRLF || newline == PCRE2_NEWLINE_ANYCRLF;

	while(1){
		r = advance(reg, str, len, utf8, crlfIsNewline);
		if(r == 0)
			break;
		if(r < 0)
			return 0;
		bufferPushReplace(L, &b, str, r, reg->o, rStr, rStrLen, &t);
	}

	luaL_addlstring(&b, str + reg->o[1], len - reg->o[1]);
	luaL_pushresult(&b);
	return 1;
}
static int l_regex_gsub(lua_State* L){
	return regSub(L, true);
}
static int l_regex_sub(lua_State* L){
	return regSub(L, false);
}

static int regMatch(lua_State* L, bool global, bool find, bool single){
	Regkex* reg = getRegex(L, 1);
	size_t len; const char* str = luaL_checklstring(L, 2, &len);
	lua_Integer off = getOffset(L, 3, len);

	int r = doMatch(L, reg, str, len, off);
	if(r == 0)
		return 0;
	if(r <= 0)
		return 2;
	if(global)
		lua_newtable(L);

	tablePushMatches(L, str, r, reg->o, find, single);
	if(!global)
		return 1;

	int m = 1;
	lua_rawseti(L, -2, m++);

	uint32_t optionsBits, newline;
	pcre2_pattern_info(reg->re, PCRE2_INFO_ALLOPTIONS, &optionsBits);
	bool utf8 = !!(optionsBits & PCRE2_UTF);
	pcre2_pattern_info(reg->re, PCRE2_INFO_NEWLINE, &newline);
	bool crlfIsNewline = newline == PCRE2_NEWLINE_ANY || newline == PCRE2_NEWLINE_CRLF || newline == PCRE2_NEWLINE_ANYCRLF;

	while(1){
		r = advance(reg, str, len, utf8, crlfIsNewline);
		if(r == 0)
			break;
		if(r < 0)
			return 0;
		tablePushMatches(L, str, r, reg->o, find, single);
		lua_rawseti(L, -2, m++);
	}

	return 1;
}
static int l_regex_gmatch1(lua_State* L){
	return regMatch(L, true, false, true);
}
static int l_regex_gmatch(lua_State* L){
	return regMatch(L, true, false, false);
}
static int l_regex_match1(lua_State* L){
	return regMatch(L, false, false, true);
}
static int l_regex_match(lua_State* L){
	return regMatch(L, false, false, false);
}
static int l_regex_gfind1(lua_State* L){
	return regMatch(L, true, true, true);
}
static int l_regex_gfind(lua_State* L){
	return regMatch(L, true, true, false);
}
static int l_regex_find1(lua_State* L){
	return regMatch(L, false, true, true);
}
static int l_regex_find(lua_State* L){
	return regMatch(L, false, true, false);
}

static int l_regex_split(lua_State* L){
	Regkex* reg = getRegex(L, 1);
	size_t len; const char* str = luaL_checklstring(L, 2, &len);
	lua_Integer off = getOffset(L, 3, len);

	lua_newtable(L);

	int r = doMatch(L, reg, str, len, off);
	if(r == 0){
		lua_pushvalue(L, 2);
		lua_rawseti(L, -2, 1);
		return 1;
	} else if(r < 0){
		return 2;
	}

	size_t m = 1;
	if(reg->o[1] > 0){
		lua_pushlstring(L, str, reg->o[0]);
		lua_rawseti(L, -2, m++);
	}

	uint32_t optionsBits, newline;
	pcre2_pattern_info(reg->re, PCRE2_INFO_ALLOPTIONS, &optionsBits);
	bool utf8 = !!(optionsBits & PCRE2_UTF);
	pcre2_pattern_info(reg->re, PCRE2_INFO_NEWLINE, &newline);
	bool crlfIsNewline = newline == PCRE2_NEWLINE_ANY || newline == PCRE2_NEWLINE_CRLF || newline == PCRE2_NEWLINE_ANYCRLF;

	size_t t = reg->o[1];
	while(1){
		r = advance(reg, str, len, utf8, crlfIsNewline);
		if(r == 0)
			break;
		if(r < 0)
			return 0;
		if(reg->o[0] > 0){
			lua_pushlstring(L, str + t, reg->o[0] - t);
			lua_rawseti(L, -2, m++);
		}
		t += reg->o[1] - t;
	}

	if(reg->o[0] < len){
		lua_pushlstring(L, str + reg->o[1], len - reg->o[1]);
		lua_rawseti(L, -2, m);
	}
	return 1;
}

static int l_regex_test(lua_State* L){
	Regkex* reg = getRegex(L, 1);
	size_t len; const char* str = luaL_checklstring(L, 2, &len);
	lua_Integer off = getOffset(L, 3, len);

	int r = doMatch(L, reg, str, len, off);
	if(r < 0)
		return 2;
	lua_pushboolean(L, r);
	return 1;
}

static int l_regex_jit(lua_State* L){
	Regkex* reg = getRegex(L, 1);
	int r = pcre2_jit_compile(reg->re, PCRE2_JIT_COMPLETE);
	if(r == 0){
		lua_pushvalue(L, 1);
		return 1;
	}
	PCRE2_UCHAR buffer[256];
	pcre2_get_error_message(r, buffer, sizeof(buffer));
	pushFail(L, (const char*)buffer);
	return 2;
}
static int l_regex_tostring(lua_State* L){
	Regkex* reg = getRegex(L, 1);
	lua_pushfstring(L, "Regkex (%p)", reg);
	return 1;
}
static int l_regex_gc(lua_State* L){
	Regkex* reg = getRegex(L, 1);
	pcre2_match_data_free(reg->md);
	pcre2_code_free(reg->re);
	return 0;
}
static const luaL_Reg l_regexlib[] = {
	{ "gsub",       l_regex_gsub },
	{ "sub",        l_regex_sub },
	{ "gmatch1",    l_regex_gmatch1 },
	{ "gmatch",     l_regex_gmatch },
	{ "match1",     l_regex_match1 },
	{ "match",      l_regex_match },
	{ "gfind",      l_regex_gfind },
	{ "gfind1",     l_regex_gfind1 },
	{ "find",       l_regex_find },
	{ "find1",      l_regex_find1 },
	{ "test",       l_regex_test },
	{ "split",      l_regex_split },
	{ "jit",        l_regex_jit },
	{ "__tostring", l_regex_tostring },
	{ "__gc",       l_regex_gc },
	{ NULL, NULL }
};

static const Flag regexFlags[] = {
	{ 'i', PCRE2_CASELESS },
	{ 'u', PCRE2_UTF },
	{ 's', PCRE2_DOTALL },
	{ 'm', PCRE2_MULTILINE },
	{ 'g', PCRE2_UNGREEDY },
	{ 0, 0 }
};
static int l_regex(lua_State* L){
	size_t len;
	int errn;
	PCRE2_SIZE erroroffset;
	const char* pat = luaL_checklstring(L, 1, &len);
	uint32_t flags = getFlags(L, 2, regexFlags, PCRE2_UTF);
	pcre2_code* re = pcre2_compile((const unsigned char*)pat, len, flags, &errn, &erroroffset, NULL);
	if(re == NULL){
		PCRE2_UCHAR buffer[256];
		pcre2_get_error_message(errn, buffer, sizeof(buffer));
		lua_pushnil(L);
		lua_pushfstring(L, "Regkex (Offset %d): %s", erroroffset, (const char*)buffer);
		return 2;
	}
	Regkex* reg = (Regkex*)lua_newuserdata(L, sizeof(Regkex));
	reg->re = re;
	reg->md = pcre2_match_data_create_from_pattern(re, NULL);
	reg->o = pcre2_get_ovector_pointer(reg->md);
	if(reg->md == NULL){
		pcre2_code_free(reg->re);
		return 0;
	}
	luaL_getmetatable(L, REG_MT);
	lua_setmetatable(L, -2);
	return 1;
}

static void appendToMetatable(lua_State* L, const struct luaL_Reg* lib){
	for(int i = 0; lib[i].func; i++){
		lua_pushcfunction(L, lib[i].func);
		lua_setfield(L, -2, lib[i].name);
	}
}
int luaopen_regkex(lua_State* L){
	luaL_newmetatable(L, REG_MT);
	lua_pushvalue(L, -1);
	lua_setfield(L, -2, "__index");
	appendToMetatable(L, l_regexlib);
	lua_pushcfunction(L, l_regex);
	return 1;
}

