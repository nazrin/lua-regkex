package.cpath = "./?.so"
local Regex = require("regkex")

local function cmp(a, b)
	if type(a) == "table" then
		for i,va in ipairs(a) do cmp(va, b[i]) end
		for i,vb in ipairs(b) do cmp(vb, a[i]) end
	else
		assert(a == b, ("%q, %q"):format(a, b))
	end
end
local function checkFail(func, ...)
	local ok, err = pcall(func, ...)
	assert(not ok)
end

checkFail(Regex)
checkFail(Regex, {})
checkFail(Regex, "", "as5d")
local r = Regex("a")
r:jit()
for i,f in ipairs({ "match", "find", "gmatch", "gfind", "match1", "find1", "gmatch1", "gfind1" }) do
	checkFail(r[f])
	checkFail(r[f], 0)
	checkFail(r[f], r, {})
	checkFail(r[f], r, "", -1)
	checkFail(r[f], r, "", 1)
	checkFail(r[f], r, "1", 2)
	checkFail(r[f], r, "1", "")
	checkFail(r[f], r, "1", {})
end
checkFail(r.sub, "")
checkFail(r.sub)
checkFail(r.sub, r)
checkFail(r.sub, r, "", 9)
checkFail(r.sub, Regex("\\d"), "a5b", function() return end)
checkFail(r.sub, Regex("\\d"), "a5b", function() return {} end)

assert(not Regex("["))
assert(tostring(Regex("")))

cmp(Regex(""):match(""), { "" })
cmp(Regex(""):match("", 0), { "" })
cmp(Regex(""):match("a", 1), { "" })
cmp(Regex(""):match("ab"), { "" })
cmp(Regex("^$"):match(""), { "" })
cmp(Regex("^$"):match("a"), nil)
cmp(Regex("a"):match("bb"), nil)
cmp(Regex("."):match("a"), { "a" })
cmp(Regex("^.$"):match("a"), { "a" })
cmp(Regex("(.)"):match("a"), { "a", "a" })
cmp(Regex("(.)."):match("ab"), { "ab", "a" })
cmp(Regex(".$"):match("ab"), { "b" })
cmp(Regex("a"):match("bab"), { "a" })
cmp(Regex("^.$"):match("Þ"), { "Þ" })
cmp(Regex("[^ab]"):match("abc"), { "c" })
cmp(Regex("\\s*(\\S*)\\s*"):match(" a "), { " a ", "a" })
cmp(Regex("\\s*(\\S*)\\s*"):match("a"), { "a", "a" })

cmp(Regex("\\s"):match("hello world"), { " " })
cmp(Regex("\\w+"):match("hello world"), { "hello" })
cmp(Regex("\\w+"):gmatch("hello world"), { {"hello"}, {"world"} })

cmp(Regex(""):gmatch("ab"), { {""}, {""}, {""} })
cmp(Regex(""):gmatch(""), { {""} })
cmp(Regex("^$"):gmatch(""), { {""} })
cmp(Regex("^$"):gmatch("a"), nil)
cmp(Regex("a"):gmatch("bb"), nil)
cmp(Regex("a"):gmatch("bab"), { {"a"} })
cmp(Regex("."):gmatch("abc"), { {"a"}, {"b"}, {"c"} })
cmp(Regex("(.)"):gmatch("abc"), { {"a","a"}, {"b","b"}, {"c","c"} })

cmp(Regex("b"):sub("bc", "B"), "Bc")
cmp(Regex("b"):sub("abc", "B"), "aBc")
cmp(Regex("b"):sub("ab", "B"), "aB")
cmp(Regex("b"):sub("a", "B"), "a")
cmp(Regex("\\d+"):sub("a659bc", "_"), "a_bc")
cmp(Regex("\\d"):sub("a659bc", "_"), "a_59bc")
cmp(Regex("\\w"):sub("ab", "_", 1), "a_")
cmp(Regex("\\w"):sub("ab", "_", 0), "_b")

cmp(Regex("\\s"):gsub("a b c", "_"), "a_b_c")
cmp(Regex("\\s"):gsub(" a b c ", "_"), "_a_b_c_")
cmp(Regex("\\s"):gsub("a  b", "_"), "a__b")
cmp(Regex("\\s"):gsub("a  b", ""), "ab")
cmp(Regex("\\s"):gsub("ab", "_"), "ab")
cmp(Regex("\\w"):gsub("ab", "_", 1), "a_")
cmp(Regex("\\w"):gsub("ab", "_", 0), "__")

cmp(Regex("(\\d)(\\d)"):gsub("10 20", function(s, a, b, i)
	assert(i == 1 or i == 4)
	return b .. a
end), "01 02")

assert(Regex("\\d"):match("fs5sl").index == 3)
assert(Regex("\\d"):gmatch("1fs5sl")[1].index == 1)
assert(Regex("\\d"):gmatch("1fs5sl")[1].index == 1)

assert(Regex("a"):test("a"))
assert(Regex("^a$"):test("a"))
assert(not Regex("a"):test("b"))
assert(not Regex("^a$"):test("aa"))
assert(Regex("a"):test("a", 0))
assert(not Regex("a"):test("a", 1))

cmp(Regex("\\w+"):match1("word"), "word")
cmp(Regex("\\w+"):match1("word", 1), "ord")
cmp(Regex("\\w+"):find1("word", 0), { 1, 4 })
cmp(Regex("\\w+"):find1("word", 1), { 2, 4 })
cmp(Regex("\\w+"):gmatch1("word"), { "word" })
cmp(Regex("\\w+"):gmatch1("word hello"), { "word", "hello" })
cmp(Regex("\\w+"):gmatch1("word hello", 1), { "ord", "hello" })
cmp(Regex("\\w+"):gfind1("word hello", 1), { { 2, 4 }, { 6, 10 } })
cmp(Regex("\\w+"):gfind1("word hello", 0), { { 1, 4 }, { 6, 10 } })

cmp(Regex("[a-z]+"):match1("word"), "word")
cmp(Regex("[a-z]+"):match1("WORD"), nil)
cmp(Regex("[a-z]+", "i"):match1("WORD"), "WORD")
cmp(Regex("[a-z]+", "i"):match1("ПРИВЕТ"), nil)
cmp(Regex("[a-z]+", "i"):find1("WORD"), { 1, 4 })
cmp(Regex("[a-z]+", "i"):sub("__WORD", ""), "__")
cmp(Regex("[a-z]+", "i"):gsub("HELLO__WORD", ""), "__")

cmp(Regex("."):gmatch1("всё"), { "в", "с", "ё" })
cmp(Regex("."):gfind1("всё"), { { 1, 2 }, { 3, 4 }, { 5, 6 } })
cmp(Regex(".", "U"):gmatch1("всё"), { '\208', '\178', '\209', '\129', '\209', '\145' })

cmp(Regex(" "):split("a b c"), { "a", "b", "c" })
cmp(Regex(" "):split("a b c", 2), { "a b", "c" })
cmp(Regex("_"):split("a b c"), { "a b c" })
cmp(Regex("a"):split("a b c"), { "", " b c" })
cmp(Regex("c"):split("a b c"), { "a b ", "" })
cmp(Regex(""):split("а б в"), { "а", " ", "б", " ", "в" })
cmp(Regex("^$"):split("а б в"), { "а б в" })

cmp(Regex(".+", ""):gmatch1("a\nb"), { "a", "b" })
cmp(Regex(".+", "s"):match1("a\nb"), "a\nb")
cmp(Regex("\\R", "s"):match1("\r\nb"), "\r\n")
cmp(Regex("^[0-9]$", "m"):gmatch1("1\n2\n3"), { "1", "2", "3" })
cmp(Regex([[/\*.*\*/]], ""):gmatch1("/* first */ code /* second */ "), { '/* first */ code /* second */' })
cmp(Regex([[/\*.*?\*/]], ""):gmatch1("/* first */ code /* second */ "), { '/* first */', '/* second */' })
cmp(Regex([[/\*.*\*/]], "g"):gmatch1("/* first */ code /* second */ "), { '/* first */', '/* second */' })

cmp(Regex("ABC", "si"):match1("abc"), "abc")
cmp(Regex("ABC", "siIi"):match1("abc"), "abc")

do
	local r = Regex("hi")
	assert(r == r:jit())
end

print("Testing huge strings (might take a bit)")
do -- Huge string test
	local str = "0"
	for i=1,26 do
		str = str .. str
	end
	str = str .. "1"
	str = str .. str
	cmp(Regex("1"):match1(str), "1")
	cmp(Regex("1.+1"):find1(str), { 67108865, 134217730 })
	assert(Regex("1.+1"):match1(str))
	cmp(Regex("0+"):split(str), { "", "1", "1" })
	assert(#Regex("1"):split(str)[1] == 67108864)
	cmp(Regex("0+"):gsub(str, ""), "11")
	assert(#Regex("1"):gsub(str, "h") == #str)
	assert(Regex("1"):gsub(str, function(m) return m end) == str)
	collectgarbage()
end

do
	local str = "01"
	for i=1,15 do
		str = str .. str
	end
	assert(#Regex("1"):split(str) == (2^15+1))
	collectgarbage()
end

