PROGRAM = regkex.so

CC     = cc
CFLAGS = -Wall -pipe -march=native -Os -std=c99
LFLAGS = -lpcre2-8 -shared
# CFLAGS += -fprofile-arcs -ftest-coverage
# LFLAGS += -fprofile-arcs
DIR    = src

SRC = $(wildcard $(DIR)/*.c)
OBJ = $(SRC:.c=.o)

LUA_VERSION = jit

ifeq ($(LUA_VERSION), jit)
CFLAGS += `pkg-config --cflags luajit || echo -I/usr/include/lua{,5.1}`
else ifeq ($(LUA_VERSION), 5.1)
CFLAGS += `pkg-config --cflags lua5.1 --silence-errors`
else ifeq ($(LUA_VERSION), 5.2)
CFLAGS += `pkg-config --cflags lua5.2 --silence-errors`
else ifeq ($(LUA_VERSION), 5.3)
CFLAGS += `pkg-config --cflags lua5.3 --silence-errors`
else ifeq ($(LUA_VERSION), 5.4)
CFLAGS += `pkg-config --cflags lua5.4 --silence-errors`
else
$(error Invalid Lua version $(LUA_VERSION))
endif

$(PROGRAM): $(OBJ)
	$(CC) $(OBJ) $(LFLAGS) -o $(PROGRAM)

%.o: %.c makefile
	$(CC) -c $(CFLAGS) $< -o $@

.PHONY: clean
clean:
	rm -fv -- $(OBJ) $(PROGRAM)

