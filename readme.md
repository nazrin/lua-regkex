# Regkex

PCRE2 Regex binding for Lua.
Aims for a comfortable and fast API while giving access to most advanced features and usecases.

## Install

One of these. Replace `5.X` with your Lua version.

* `make LUA_VERSION=5.X && sudo mkdir /usr/lib/lua/5.X/ && sudo cp regkex.so /usr/lib/lua/5.X/`
* `sudo luarocks install regkex --lua-version=5.X`
* `luarocks install regkex --local --lua-version=5.X`

## API

Functions error with invalid parameters, `nil` on no match, and return `nil, msg` on other errors.

### Base

```
Regex Regex(string pattern[, string flags])
```

| Flag | Name         | Description                                     |
| ---- | ------------ | ----------------------------------------------- |
| `i`  | Ignore Case  | Case-insensitive search.                        |
| `s`  | Dot All      | Allows `.` to match newline characters.         |
| `m`  | Multiline    | Allows `^` and `$` to match newline characters. |
| `g`  | Ungreedy     | Quantifiers are not greedy by default.          |
| `U`  | Disable UTF8 | Ignore UTF8 codepoints.                         |

------------

### Regex

| Function                                   | Description                                                         |
| ------------------------------------------ | ------------------------------------------------------------------- |
| `Regex:match(str[, offset])`               | Return matches.                                                     |
| `Regex:find(str[, offset])`                | Return indexes of matches.                                          |
| `Regex:sub(str, replacement[, offset])`    | Substitute occurrence and return result.                            |
| `Regex:test(str[, offset])`                | Tests for any occurrence.                                           |
| `Regex:split(str[, offset])`               | Splits the string into an array.                                    |
| `Regex:jit()`                              | JIT compile for faster execution in exchange for slow compilation.  |

| Modifier | Affects                | Description                        |
| -------- | ---------------------- | ---------------------------------- |
| `-1`     | `match`, `find`        | Only return full match, no groups. |
| `g-`     | `match`, `find`, `sub` | Match multiple results.            |

#### Full signatures

```
[{ string match[, string group...] }]        Regex:match(string str[, int offset])
[{ { string match[, string group...] }... }] Regex:gmatch(string str[, int offset])
[ string match ]                             Regex:match1(string str[, int offset])
[{ string match }...]                        Regex:gmatch1(string str[, int offset])

[{ { int s, int e }... }]        Regex:find(string str[, int offset])
[{ { { int s, int e }... }... }] Regex:gfind(string str[, int offset])
[{ int s, int e }]               Regex:find1(string str[, int offset])
[{ { int s, int e }... }]        Regex:gfind1(string str[, int offset])

string Regex:sub(string str, string|function replacement[, int offset])
string Regex:gsub(string str, string|function replacement[, int offset])

true|false Regex:test(string str[, int offset])

{ str[, str]... } Regex:split(string str[, int offset])

Regex Regex:jit()
```

## Example

```lua
local Regex = require("regkex")

Regex("\\d"):match("a1") --> { '1', index = 2 }
Regex("0?(\\d)"):match("01") --> { '01', '1', index = 1 }

Regex("привет", "i"):match("ПРИВЕТ") --> { 'ПРИВЕТ', index = 1 }
Regex("привет", "i"):match1("ПРИВЕТ") --> 'ПРИВЕТ'

Regex("\\w+"):gmatch("hello, world!") --> { { 'hello', index = 1 }, { 'world', index = 8 } }
Regex("(\\w)\\d"):gmatch("a1 b2") --> { { 'a1', 'a', index = 1 }, { 'b2', 'b', index = 4 } }
Regex("(\\w)\\d"):gmatch1("a1 b2") --> { 'a1', 'b2' }

Regex("\\s+"):sub("a b c", "") --> 'ab c'
Regex("\\s+"):gsub("a b c", "") --> 'abc'
Regex("(\\d)(\\d)"):gsub("10 20", function(str, a, b, index)
	return b .. a
end) --> '01 02'

Regex([[/\*.*\*/]], "g"):gmatch1("/* first */ code /* second */ ") --> { '/* first */', '/* second */' })

local wordTest = Regex[[^\w+$]] -- [[]] Syntax might be nicer in some cases
wordTest:jit() -- Slow to compile but faster to execute, useless in this example
wordTest:test("hello") --> true
wordTest:test("hello!") --> false

```
